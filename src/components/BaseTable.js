import { Table } from 'ant-design-vue'

export const BaseTable = {
  computed: {
    tableData() {
      const { dataSource } = this.$attrs

      return dataSource.map((item, index) => {
        item._rowKey = index
        return item
      })
    }
  },

  render() {
    const { $scopedSlots, $listeners, tableData, $attrs } = this

    const config = {
      props: { ...$attrs, dataSource: tableData, rowKey: '_rowKey' },
      on: { ...$listeners },
      scopedSlots: $scopedSlots
    }

    return <Table {...config}></Table>
  }
}
