export default [
  {
    path: 'student/course',
    component: () =>
      import(
        /* webpackChunkName:"student"*/ '../views/Student/CourseManage/CourseManage.vue'
      ),
    meta: {
      requireAuth: true,
      permission: ['student']
    }
  },
  {
    path: 'student/score',
    component: () =>
      import(
        /* webpackChunkName:"student"*/ '../views/Student/ScoreManage/ScoreManage.vue'
      ),
    meta: {
      requireAuth: true,
      permission: ['student']
    }
  }
]
