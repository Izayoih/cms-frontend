export default [
  {
    path: 'admin/course',
    component: () =>
      import(
        /* webpackChunkName:"admin"*/ '../views/Admin/CourseManage/CourseManage.vue'
      ),
    meta: {
      requireAuth: true,
      permission: ['admin']
    }
  },
  {
    path: 'admin/student',
    component: () =>
      import(
        /* webpackChunkName:"admin"*/ '../views/Admin/StudentManage/StudentManage.vue'
      ),
    meta: {
      requireAuth: true,
      permission: ['admin']
    }
  },
  {
    path: 'admin/teacher',
    component: () =>
      import(
        /* webpackChunkName:"admin"*/ '../views/Admin/TeacherManage/TeacherManage.vue'
      ),
    meta: {
      requireAuth: true,
      permission: ['admin']
    }
  }
]
