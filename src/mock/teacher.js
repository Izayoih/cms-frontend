import Mock, { Random } from 'mockjs'
import qs from 'qs'

const courseList = Mock.mock({
  'data|5': [
    {
      'id|+1': 1,
      courseName: () => Random.cword(4, 10)
    }
  ]
})

Mock.mock(/api\/teacher\/courseList/, 'get', () => {
  return {
    success: true,
    data: courseList.data
  }
})

const recordList = Mock.mock({
  'data|37': [
    {
      'id|+1': 1,
      'courseId|+1': 1,
      courseName: () => Random.cword(4, 10),
      studentName: () => Random.name(),
      'score|0-100': 1,
      message: () => Random.cword(4, 10)
    }
  ]
})

Mock.mock(/api\/teacher\/recordList/, 'get', options => {
  const params = qs.parse(options.url.split('?')[1])
  const page = Number(params.page)

  const result = {
    success: true,
    total: recordList.data.length,
    page,
    data: recordList.data.slice((page - 1) * 10, page * 10)
  }

  return result
})

Mock.mock('/api/teacher/editScore', 'post', options => {
  const params = JSON.parse(options.body)
  const id = Number(params.id)
  const message = params.message
  const score = Number(params.score)

  const target = recordList.data.find(item => item.id === id)
  target.score = score
  target.message = message

  return {
    success: true,
    msg: ''
  }
})
