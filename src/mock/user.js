import Mock, { Random } from 'mockjs'

const personalInfo = Mock.mock({
  'id|+1': 1,
  'userType|2-3': 1,
  name: () => Random.name(),
  sex: () => Random.cword('男女'),
  phone: /^1\d{10}$/,
  rank: '教师',
  number: () => Random.string()
})

Mock.mock('/api/user/personalInfo', 'get', {
  success: true,
  data: personalInfo
})

Mock.mock('/api/user/editPersonalInfo', 'post', options => {
  const params = JSON.parse(options.body)
  personalInfo.phone = params.phone

  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/user/login', 'post', () => {
  return {
    success: true,
    data: {
      id: 1,
      name: 'abc'
    }
  }
})

Mock.mock('/api/user/logout', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})

Mock.mock('/api/user/resetPwd', 'post', () => {
  return {
    success: true,
    msg: ''
  }
})
