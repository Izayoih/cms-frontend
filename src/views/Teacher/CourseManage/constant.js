export const searchLayout = [
  [
    {
      type: 'select',
      placeholder: '课程',
      field: 'courseID',
      options: []
    },
    {
      type: 'input',
      placeholder: '学生姓名',
      field: 'studentName'
    }
  ]
]

export const columns = [
  {
    dataIndex: 'courseID',
    title: '课程ID'
  },
  {
    dataIndex: 'courseName',
    title: '课程名'
  },
  {
    dataIndex: 'studentName',
    title: '学生姓名'
  },
  {
    dataIndex: 'score',
    title: '分数'
  },
  {
    dataIndex: 'message',
    title: '评价'
  },
  {
    dataIndex: 'action',
    title: '操作',
    scopedSlots: { customRender: 'action' }
  }
]
