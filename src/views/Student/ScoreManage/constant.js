export const searchLayout = [
  [
    {
      type: 'input',
      placeholder: '课程名',
      field: 'courseName'
    },
    {
      type: 'input',
      placeholder: '课程ID',
      field: 'courseID'
    }
  ]
]

export const columns = [
  {
    dataIndex: 'id',
    title: '课程ID'
  },
  {
    dataIndex: 'courseName',
    title: '课程名'
  },
  {
    dataIndex: 'teacherName',
    title: '主讲老师'
  },
  {
    dataIndex: 'score',
    title: '分数'
  },
  {
    dataIndex: 'message',
    title: '评价'
  }
]
