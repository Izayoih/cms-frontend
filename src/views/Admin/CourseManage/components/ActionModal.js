import { Modal, Form, Input, Select } from 'ant-design-vue'

export const ActionModal = Form.create({})({
  props: {
    visible: Boolean,
    loading: Boolean,
    dataSource: {
      type: Object,
      default: () => ({})
    }
  },

  methods: {
    handleCancel() {
      this.form.resetFields()
      this.$emit('close')
    },

    handleSubmit() {
      const { form, dataSource } = this
      form.validateFields((errors, values) => {
        if (!errors) {
          const data = {
            ...dataSource,
            ...values
          }

          this.$emit('submit', { form, data })
        }
      })
    }
  },

  render() {
    const {
      visible,
      loading,
      dataSource: { courseName, courseIntro, teacherId },
      form: { getFieldDecorator }
    } = this

    const teacherList = this.dataSource.teacherList || []

    return (
      <Modal
        visible={visible}
        title="课程"
        onCancel={this.handleCancel}
        onOk={this.handleSubmit}
        confirmLoading={loading}
      >
        <Form labelCol={{ span: 8 }} wrapperCol={{ span: 12 }}>
          <Form.Item label="课程名">
            {getFieldDecorator('courseName', {
              initialValue: courseName || '',
              rules: [{ required: true, message: '请输入课程名' }]
            })(<Input />)}
          </Form.Item>

          <Form.Item label="主讲老师">
            {getFieldDecorator('teacherId', {
              initialValue: teacherId,
              rules: [{ required: true, message: '请选择主讲老师' }]
            })(
              <Select>
                {teacherList.length > 0 &&
                  teacherList.map(item => {
                    return (
                      <SelectOption value={item.value}>
                        {item.label}
                      </SelectOption>
                    )
                  })}
              </Select>
            )}
          </Form.Item>

          <Form.Item label="课程介绍">
            {getFieldDecorator('courseIntro', {
              initialValue: courseIntro || '',
              rules: [{ max: 200, message: '字数不得超过200字' }]
            })(<Input.TextArea />)}
          </Form.Item>
        </Form>
      </Modal>
    )
  }
})
