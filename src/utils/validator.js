// 表单校验器

// 电话校验
export const phoneValidator = (rule, value, callback) => {
  const reg = /^1\d{10}$/
  if (!reg.test(value)) {
    return callback(new Error('请输入正确的号码'))
  }
  callback()
}

// 密码校验
export const pwdValidator = (rule, value, callback) => {
  const reg = /^[0-9a-zA-Z]{8,16}$/
  if (!reg.test(value)) {
    return callback(new Error('请输入8-16位字母或数字'))
  }
  callback()
}
