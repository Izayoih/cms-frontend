export const searchMixin = {
  data() {
    return {
      searchInfo: {},

      mainTable: {
        loading: true,
        pagination: { current: 1, total: 0 }
      }
    }
  },

  methods: {
    handleSearch() {
      this.searchInfo = this.$refs.searchBar.searchInfo
      this.getList()
    },

    handleTableChange({ current }) {
      this.mainTable.pagination.current = current
      this.getList()
    }
  }
}
