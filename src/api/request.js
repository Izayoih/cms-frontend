import axios from 'axios'
import { message } from 'ant-design-vue'
import { Storage } from '@/utils/storage'
import router from '@/router/index'

const instance = axios.create({})

instance.interceptors.response.use(response => {
  response = response.data

  const { code, msg } = response

  if (code === 4003 || code === 4001) {
    message.error(msg)

    // 登录态失效
    if (code === 4001) {
      Storage.remove('_user')
      router.push({ path: '/account/login' })
    }
  }

  return response
})

export const request = async (method, url, data) => {
  method = method.toUpperCase()
  const config = {
    method,
    url
  }

  switch (method) {
    case 'GET': {
      config.params = data
      break
    }
    case 'POST': {
      config.data = data
      break
    }
  }

  return await instance(config)
}
