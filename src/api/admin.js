import { request } from './request'

// 获取课程列表
export const apiGetCourseList = data => {
  return request('GET', '/api/admin/courseList', data)
}

// 新增课程
export const apiAddCourse = data => {
  return request('POST', '/api/admin/addCourse', data)
}

// 修改课程
export const apiEditCourse = data => {
  return request('POST', '/api/admin/editCourse', data)
}

// 删除课程
export const apiDelCourse = data => {
  return request('POST', '/api/admin/delCourse', data)
}

// 获取学生列表
export const apiGetStudentList = data => {
  return request('GET', '/api/admin/studentList', data)
}

// 新增学生
export const apiAddStudent = data => {
  return request('POST', '/api/admin/addStudent', data)
}

// 修改学生
export const apiEditStudent = data => {
  return request('POST', '/api/admin/editStudent', data)
}

// 删除学生
export const apiDelStudent = data => {
  return request('POST', '/api/admin/delStudent', data)
}

// 获取教师列表
export const apiGetTeacherList = data => {
  return request('GET', '/api/admin/teacherList', data)
}

// 新增教师
export const apiAddTeacher = data => {
  return request('POST', '/api/admin/addTeacher', data)
}

// 修改教师
export const apiEditTeacher = data => {
  return request('POST', '/api/admin/editTeacher', data)
}

// 删除教师
export const apiDelTeacher = data => {
  return request('POST', '/api/admin/delTeacher', data)
}
