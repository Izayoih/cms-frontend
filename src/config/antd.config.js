import {
  ConfigProvider,
  Layout,
  Menu,
  Breadcrumb,
  Row,
  Col,
  Select,
  Input,
  Button,
  Table,
  Divider,
  Modal,
  message,
  Descriptions,
  Form,
  Icon,
  Dropdown
} from 'ant-design-vue'

export const assignAntd = Vue => {
  Vue.component('ConfigProvider', ConfigProvider)

  Vue.component('Button', Button)
  Vue.component('Table', Table)

  Vue.component('Layout', Layout)
  Vue.component('LayoutSider', Layout.Sider)
  Vue.component('LayoutHeader', Layout.Header)
  Vue.component('LayoutContent', Layout.Content)

  Vue.component('Menu', Menu)
  Vue.component('MenuItem', Menu.Item)
  Vue.component('MenuDevider', Menu.Divider)
  Vue.component('SubMenu', Menu.SubMenu)

  Vue.component('Breadcrumb', Breadcrumb)
  Vue.component('BreadcrumbItem', Breadcrumb.Item)

  Vue.component('Row', Row)
  Vue.component('Col', Col)

  Vue.component('Select', Select)
  Vue.component('SelectOption', Select.Option)
  Vue.component('Input', Input)
  Vue.component('InputPassword', Input.Password)

  Vue.component('Divider', Divider)

  Vue.use(Modal)
  Vue.component('Modal', Modal)

  Vue.component('Descriptions', Descriptions)
  Vue.component('DescriptionsItem', Descriptions.Item)

  Vue.use(Form)
  Vue.component('Form', Form)
  Vue.component('FormItem', Form.Item)

  Vue.component('Icon', Icon)

  Vue.component('Dropdown', Dropdown)

  Vue.prototype.$confirm = Modal.confirm
  Vue.prototype.$success = Modal.success
  Vue.prototype.$message = message
}
