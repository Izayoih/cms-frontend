import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { assignAntd } from './config/antd.config'
import { assignComponents } from './config/component.config'

// import './mock/student'
// import './mock/teacher'
// import './mock/admin'
// import './mock/user'

assignAntd(Vue)
assignComponents(Vue)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
